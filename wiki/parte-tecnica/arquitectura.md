# Arquitectura de LoB

LoB se basa en una arquitectura similar a la de DDD sin atarse a ella, aquí podemos ver un esquema de la situación de los componentes usados en DDD:
![image](/uploads/f535d56ddf42983cbb4b08f8b3585994/image.png)

## Arquitectura orientada a servicios
El código de la aplicación se encapsula en servicios, dichos servicios pueden estar en las distintas capas de la aplicación.

## El dominio de una aplicación
El dominio de una aplicación es la conjunción de la definición de las entidades de negocio y los propios servicios de negocio. En la capa de dominio se habla lo que se denomina el lenguaje ubicuo:  
![image](/uploads/d09aa315aaa5303317e6154dffe74c50/image.png)

El lenguaje ubicuo se define como la *jerga* en común entre los expertos funcionales y los técnicos que desarrollan la aplicación.

## Arquitectura basada en capas

* [Capa de infraestructura](#capa-de-infraestructura)
* [Capa de dominio](#capa-de-dominio)
* [Capa de aplicación](#capa-de-aplicación)
* [Capa de presentación](#capa-de-presentación)

# Capa de infraestructura
Es la capa donde se albergan los servicios de infraestructura. Dichos servicios son aquellos que tienen dependencias de librerías de terceros que trabajan con recursos externos al dominio, recursos del sistema.  
La capa de acceso a datos es una capa de infraestructura debido a que se encarga de persistir datos en un sistema de base de datos.  
Un servicio que genera PDFs usando una librería de un tercero es un servicio de infraestructura por que depende de una librería externa. 
El servicio que guarda en disco los archivos contenidos en un documento de la gestión documental es un servicio de infraestructura debido a que escribe en archivos de disco.

# Capa de dominio
Es la capa donde se define el dominio de una aplicación.  
Cualquier método de un servicio de dominio podrá ser entendido por los expertos funcionales cuando este conoce el lenguaje ubicuo, sin tener que conocer detalles técnicos de implementación.  
En la capa de dominio no se escriben instrucciones técnicas, no se inician o terminan transacciones, en la capa de dominio solo se conocen los objetos del negocio, sus propiedades y sus relaciones. Y se realizan operaciones sobre ellos y los conjuntos de ellos.

# Capa de aplicación
Es la capa encargada de coordinar la capa de presentación con la capa de dominio. Es la encargada de trabajar con [DTOs](https://es.wikipedia.org/wiki/Objeto_de_Transferencia_de_Datos_(DTO)), traduce los datos de entrada y generar los datos de salida.  
También es la encargada de coordinar las transacciones entre distintos métodos de dominio.
Cada método de un servicio de aplicación se suele corresponder con un caso de uso de la aplicación.

# Capa de presentación
Es la capa donde se introduce el código necesario para interactuar con el usuario. Escucha las peticiones del usuario y responde presentando datos en vistas.  
En caso de cliente pesado serían las ventanas WinForm, en caso de ser una aplicación web, la capa de presentación incluye vistas, controladores, view models y también código de cliente web.

